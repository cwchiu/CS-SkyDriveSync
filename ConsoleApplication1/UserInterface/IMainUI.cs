﻿using System;
using System.Collections.Generic;
using System.Text;
using API.WindowsLive.SkyDrive.Events;

namespace API.WindowsLive.SkyDrive.UserInterface
{
    /// <summary>
    /// 操作介面
    /// </summary>
    interface IMainUI
    {
        void FileChanged(Object sender, FileChangedEventArgs e);
        void ExceptionRaised(Object sender, ExceptionEventArgs e);

        void ScheduleStarted(Object sender, EventArgs e);
        void ScheduleEnded(Object sender, EventArgs e);
        void ScheduleWaiting(Object sender, EventArgs e);
    }
}

