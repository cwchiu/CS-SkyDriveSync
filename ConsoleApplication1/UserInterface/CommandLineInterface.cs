﻿using System;
using System.Collections.Generic;
using System.Text;
using API.WindowsLive.SkyDrive.Events;

namespace API.WindowsLive.SkyDrive.UserInterface
{
    /// <summary>
    /// 命令模式 UI
    /// </summary>
    class CommandLineInterface : IMainUI
    {
        int cursorPosition = 0;

        public void FileChanged(Object sender, FileChangedEventArgs e)
        {
            switch (e.EventType)
	        {
                case FileChangedEventArgs.EventTypeEnum.downloadProgress:
                    downloadProgress(e);
                    break;
                case FileChangedEventArgs.EventTypeEnum.download:
                    startDowload(e);
                    break;
                case FileChangedEventArgs.EventTypeEnum.downloadCompleted:
                    downloadCompleted(e);
                    break;
                case FileChangedEventArgs.EventTypeEnum.upload:
                    startUpload(e);
                    break;
                case FileChangedEventArgs.EventTypeEnum.uploadCompleted:
                    uploadCompleted(e);
                    break;
                case FileChangedEventArgs.EventTypeEnum.uploadProgress:
                    uploadProgress(e);
                    break;
                case FileChangedEventArgs.EventTypeEnum.addForDownload:
                    addForDownload(e);
                    break;
                case FileChangedEventArgs.EventTypeEnum.addForUpload:
                    addForUpload(e);
                    break;
                case FileChangedEventArgs.EventTypeEnum.deleteLocalFile:
                    deleteLocalFile(e);
                    break;
                case FileChangedEventArgs.EventTypeEnum.deleteLocalFolder:
                    deleteLocalFolder(e);
                    break;
                case FileChangedEventArgs.EventTypeEnum.deleteWebFile:
                    deleteRemoteFile(e);
                    break;
                case FileChangedEventArgs.EventTypeEnum.deleteWebFolder:
                    deleteRemoteFolder(e);
                    break;
                default:
                    Console.WriteLine("File changed unhandled");
                    break;
	        }
        }

        public void ExceptionRaised(Object sender, ExceptionEventArgs e)
        {
            Console.WriteLine("Exception occured @: " + e.ExceptionLocation  + "\n " + e.Exception.Message);
        }

        public void ScheduleStarted(Object sender, EventArgs e)
        {
            Console.WriteLine("Start schedule");
        }

        public void ScheduleEnded(Object sender, EventArgs e)
        {
            Console.WriteLine("End schedule");
        }

        public void ScheduleWaiting(Object sender, EventArgs e)
        {
            Console.WriteLine("Waiting ...");
        }

        private void downloadProgress(FileChangedEventArgs e)
        {
            Console.CursorLeft = cursorPosition;
            Console.Write(String.Format("{0}%", (int)((float)e.TransferSoFar / (float)e.TotalSize * 100)));
        }

        private void uploadProgress(FileChangedEventArgs e)
        {
            Console.CursorLeft = cursorPosition;
            Console.Write(String.Format("{0}%", (int)((float)e.TransferSoFar / (float)e.TotalSize * 100)));
        }

        private void startDowload(FileChangedEventArgs e)
        {
            Console.WriteLine("Start to download " + e.FileName);
            Console.Write("Progress =  ");
            cursorPosition = Console.CursorLeft;
            Console.Write("0%");
        }

        private void startUpload(FileChangedEventArgs e)
        {
            Console.WriteLine("Start to upload " + e.FileName);
            Console.Write("Progress = ");
            cursorPosition = Console.CursorLeft;
            Console.Write("0%");
        }

        private void downloadCompleted(FileChangedEventArgs e)
        {
            Console.CursorLeft = cursorPosition;
            Console.Write("100%");
            Console.WriteLine();
        }

        private void uploadCompleted(FileChangedEventArgs e)
        {
            Console.CursorLeft = cursorPosition;
            Console.Write("100%");
            Console.WriteLine();
        }

        private void addForDownload(FileChangedEventArgs e)
        {
            Console.WriteLine(String.Format("Adding {0} for download", e.FileName));
        }

        private void addForUpload(FileChangedEventArgs e)
        {
            Console.WriteLine(String.Format("Adding {0} for upload", e.FileName));
        }

        private void deleteLocalFile(FileChangedEventArgs e)
        {
            Console.WriteLine(String.Format("Deleting local file {0}", e.FileName));
        }

        private void deleteLocalFolder(FileChangedEventArgs e)
        {
            Console.WriteLine(String.Format("Deleting local folder {0}", e.FileName));
        }

        private void deleteRemoteFile(FileChangedEventArgs e)
        {
            Console.WriteLine(String.Format("Deleting remote file {0}", e.FileName));
        }

        private void deleteRemoteFolder(FileChangedEventArgs e)
        {
            Console.WriteLine(String.Format("Deleting remote folder {0}", e.FileName));
        }


    }
}
