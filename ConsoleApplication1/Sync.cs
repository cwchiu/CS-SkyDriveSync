﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.IO;

namespace Sync
{
    class Synchronizer3
    {
        private String startFolder = "";
        private String destinationFolder = "";
        int waitingTime = 0;
        int totalNumberOfFiles = 0;
        int copiedSoFar = 0;
        List<String> filesToCopy = new List<String>();

        public Synchronizer3(String startFolder, String destinationFolder, int waitingTime)
        {
            this.startFolder = startFolder;
            this.destinationFolder = destinationFolder;
            this.waitingTime = waitingTime;

            checkFiles(startFolder, destinationFolder);

            totalNumberOfFiles = filesToCopy.Count;
            Console.WriteLine("total number of files to copy is " + totalNumberOfFiles);
            processDirectory(startFolder, destinationFolder, waitingTime);
        }

        private bool checkIfFileAvailable(String orginalDiretory, String originalLocation, String destinationDirectory)
        {
            String newFileLocation = originalLocation.Replace(orginalDiretory, destinationDirectory);
            return File.Exists(newFileLocation);
        }

        private void copyAndWait(String orginalFile, String destinationFile, int waitTime)
        {
            File.Copy(orginalFile, destinationFile);
            Thread.Sleep(waitTime);
        }

        private void checkFiles(String orginalDiretory, String destinationDiretory)
        {
            DirectoryInfo di = new DirectoryInfo(orginalDiretory);

            FileInfo[] files = di.GetFiles();
            DirectoryInfo[] directories = di.GetDirectories();


            foreach (FileInfo fileInfo in files)
                if (!checkIfFileAvailable(orginalDiretory, fileInfo.FullName, destinationDiretory))
                    filesToCopy.Add(fileInfo.FullName);

            foreach (DirectoryInfo directoryInfo in directories)
                if (!directoryInfo.FullName.Equals(destinationFolder))
                    checkFiles(directoryInfo.FullName, directoryInfo.FullName.Replace(orginalDiretory, destinationDiretory));

        }//End of checkFiles

        private void processDirectory(String orginalDiretory, String destinationDiretory, int waitingTime)
        {
            DirectoryInfo di = new DirectoryInfo(orginalDiretory);

            FileInfo[] files = di.GetFiles();
            DirectoryInfo[] directories = di.GetDirectories();

            if (!Directory.Exists(destinationDiretory))
                Directory.CreateDirectory(destinationDiretory);

            foreach (FileInfo fileInfo in files)
                if (!checkIfFileAvailable(orginalDiretory, fileInfo.FullName, destinationDiretory))
                {
                    copiedSoFar++;
                    Console.WriteLine("Copying " + fileInfo.FullName + "\n** " + copiedSoFar + " of " + totalNumberOfFiles + " **");
                    copyAndWait(fileInfo.FullName, fileInfo.FullName.Replace(orginalDiretory, destinationDiretory), waitingTime);
                }

            foreach (DirectoryInfo directoryInfo in directories)
                if (!directoryInfo.FullName.Equals(destinationFolder))
                    processDirectory(directoryInfo.FullName, directoryInfo.FullName.Replace(orginalDiretory, destinationDiretory), waitingTime);

        }//End of processing method

    }//End of class

}//End of namespace
