﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using HgCo.WindowsLive.SkyDrive;
using API.WindowsLive.SkyDrive.Events;

namespace API.WindowsLive.SkyDrive.BusinessLogic
{
    /// <summary>
    /// SkyDrive 核心
    /// </summary>
    /// <seealso cref="SkyDriveWebClient"/>
    partial class SkyDriveLogic
    {
        const char webDirectorySeperator = '/';

        private WebFolderInfo webSyncRootDirectory = null;
        private DirectoryInfo localSyncRootDirectory = null;

        List<FileInfo> filesToUpload = new List<FileInfo>();
        List<WebFileInfo> filesToDownload = new List<WebFileInfo>();

        private Boolean updateLocalFiles = true;
        private Boolean updateRemoteFiles = true;
        long waitTicksBeforeRaiseProgressEvent = 100 * TimeSpan.TicksPerMillisecond; // 0.5 seconds

        String userName, password, destinationDirectory;

        //Not using {get; set;} as it is not compatible with VS2K5
        public Boolean UpdateLocalFiles
        {
            get { return this.updateLocalFiles; }
            set { this.updateLocalFiles = value; }
        }

        public Boolean UpdateRemoteFiles
        {
            get { return this.updateRemoteFiles; }
            set { this.updateRemoteFiles = value; }
        }

        SkyDriveWebClient skyDrive;
        SplitLargeFiles largeFileSplitter;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName">帳號</param>
        /// <param name="password">密碼</param>
        /// <param name="timeout"></param>
        /// <param name="maxFileSize">檔案分割大小</param>
        /// <param name="localDirectory">本地同步資料夾</param>
        /// <param name="destinationDirectory">遠端同步資料夾</param>
        /// <param name="webProxy">代理</param>
        public SkyDriveLogic(String userName, String password, int timeout, long maxFileSize, DirectoryInfo localDirectory, String destinationDirectory, IWebProxy webProxy)
        {
            if (destinationDirectory.Equals("\\") || destinationDirectory.Equals(String.Empty))
                throw new InvalidDataException("Destination folder cannot be root folder");

            skyDrive = new SkyDriveWebClient();

            if (webProxy != null)
                skyDrive.Proxy = webProxy;
                        
            skyDrive.Timeout = timeout;

            this.localSyncRootDirectory = localDirectory;
            localSyncRootDirectory.Create();

            largeFileSplitter = new SplitLargeFiles(maxFileSize);

            this.userName = userName;
            this.password = password;
            this.destinationDirectory = destinationDirectory;
        }

        public SkyDriveLogic(String userName, String password, int timeout, long maxFileSize, String localDirectory, String webDirectory, IWebProxy webProxy)
            : this(userName, password, timeout, maxFileSize, new DirectoryInfo(localDirectory), webDirectory, webProxy)
        {
        }

        /// <summary>
        /// 登入
        /// </summary>
        public void login()
        {
            try
            {
                skyDrive.LogOn(userName, password);
            }
            catch (LogOnFailedException ex)
            {
                throw new LogOnFailedException("Could not log on, please check username and password.\n" + ex.Message);
            }
            catch
            {
                throw;
            }

            webSyncRootDirectory = checkAndCreateWebFolders(destinationDirectory);
        }

        /// <summary>
        /// 
        /// </summary>
        public void WarmUp()
        {
            WebFolderInfo[] folders = null;
            WebFileInfo[] files = null;

            while (folders == null)
            {
                try
                {
                    folders = skyDrive.ListRootWebFolders();

                    if(folders.Length > 0)
                        files = skyDrive.ListSubWebFolderFiles(folders[0]);
                }
                catch (Exception ex)
                {
                    ExceptionRaised(this, new ExceptionEventArgs(ex, ExceptionEventArgs.ExceptionLocationEnum.warmingUp));
                }
            }
        }

        # region deleguates and events

        public delegate void FileChangedHandler(object sender, FileChangedEventArgs e);
        public event FileChangedHandler FileChanged;

        public delegate void ExceptionHandler(object sender, ExceptionEventArgs e);
        public event ExceptionHandler ExceptionRaised;

        # endregion


    }//End of class

}//End of namespace
