﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.WindowsLive.SkyDrive.Events
{
    class FileChangedEventArgs : EventArgs
    {
        public enum EventTypeEnum { download, upload, addForDownload, addForUpload, downloadCompleted, uploadCompleted, deleteLocalFile, deleteWebFile, deleteLocalFolder, deleteWebFolder, downloadProgress, uploadProgress};

        String fullFileName, fileName;
        long totalSize, transferSoFar;
        EventTypeEnum eventType; 

        public FileChangedEventArgs(String fullFileName, String fileName, long transferSoFar, long totalSize, EventTypeEnum eventType)
        {
            this.fullFileName = fullFileName;
            this.fileName = fileName;
            this.transferSoFar = transferSoFar;
            this.totalSize = totalSize;
            this.eventType = eventType;
        }

        public String FullFileName
        {
            get { return this.fullFileName; }
            set { this.fullFileName = value; }
        }

        public String FileName
        {
            get { return this.fileName; }
            set { this.fileName = value; }
        }

        public long TotalSize
        {
            get { return this.totalSize; }
            set { this.totalSize = value; }
        }

        public long TransferSoFar
        {
            get { return this.transferSoFar; }
            set { this.transferSoFar = value; }
        }

        public EventTypeEnum EventType
        {
            get { return this.eventType; }
            set { this.eventType = value; }
        }
    }

    class ExceptionEventArgs : EventArgs
    {
        Exception exception;
        ExceptionLocationEnum exceptionLocation;

        public enum ExceptionLocationEnum{ download, upload, warmingUp};

        public ExceptionEventArgs(Exception exception, ExceptionLocationEnum exceptionLocation)
        {
            this.exception = exception;
            this.exceptionLocation = exceptionLocation;
        }

        public Exception Exception
        {
            get { return this.exception; }
            set { this.exception = value; }
        }

        public ExceptionLocationEnum ExceptionLocation
        {
            get { return this.exceptionLocation; }
            set { this.exceptionLocation = value; }
        }
    }

}
